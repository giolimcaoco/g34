/*
		Node.js

		Limitations
			Node.js does not have direct support for common web dev tasks such as:
				handling HTTP verbs (GET, POST, etc)
				handling requests at different paths routing
				serving of static files
			this means that using Node.js on its own for web dev may get tedious

				Instead of writing code for these web dev tasks, use web framework such as Express.js

		Express.js

			An open-source, unopinionated web application framework written in JS and hosted in Node.js
				Mantra: As long as it works, you'll be fine


			Web Framework
				Set of components designed to simplify the web dev process
					convention over configuration
				Allows dev to focus on business logic of their app
				Comes in two forms: opinionated (follows set codes) and unopinionated (build your code)

			Opinionated Web Framework
				The framework dictates how it should be used by the dev
				speeds up the startup process of app dev
				enforces best practices for the frameworks use case
				lack of flexibility could be a drawback when apps needs are not aligned with the framework assumptions

			Unopinionated Web Framework
				The dev dictates how to use the framework
				Offers flexibility to create an application unbound by any use case
				No "right way" of structuring an application
				abundance of options may be overwhelming

				Advantages over plain Node.js
					Simplicity makes it easy to learn and use


*/

